const { app, BrowserWindow, ipcMain, dialog } = require('electron');

let win;

function createWindow() {
  win = new BrowserWindow({
    height: 600,
    width: 800,
    webPreferences: {
      nodeIntegration: true
    }
  });

  win.loadFile('index.html');

  // win.webContents.openDevTools();

  win.on('closed', () => {
    win = null;
  });
}

app.on('ready', createWindow);

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit();
  }
});

app.on('activate', () => {
  if (win === null) {
    createWindow();
  }
});

ipcMain.on('save-dialog', event => {
  const options = {
    title: '保存模型',
    filters: [
      {
        name: 'Models',
        extensions: ['gltf', 'glb']
      }
    ]
  };
  dialog.showSaveDialog(options).then(obj => {
    event.sender.send('saved-file', obj);
  });
});

ipcMain.on('open-error-dialog', (event, message) => {
  dialog.showErrorBox('错误', message);
});
